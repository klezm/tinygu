
export const TRANSLATIONS = {
  "de": {
    "Login": "Anmelden",
    "Shorten": "Kürzen",
    "Account": "Account",
    "Help": "Hilfe",
    "About": "Über Uns",
    "More Options": "Mehr",
    "Custom shortlink (optional)": "Eigene URL (Optional)",
    "Add to Account": "Zu Account hinzufügen",
    "Language": "Sprache",

  }
};
